### Running the program

```bash
python run.py
```

Above command will set up a simple flask server listening on `http://localhost:5000`.
It will serve the frontend located on frontend directory.

To create reports of test just make a curl call:
```bash
 curl http://127.0.0.1:5000/report
```

### How to `config.yml`

On `config.yml` you can configure all the tests to do. Available tests are:
* Internal tests (made on the own computer):
    - `Systemctl` call
    - `ps cax` call
* External tests (made on external computer):
    - `ping`
    - `ping6`

Check on `tests/` to see the name of each test. Or look at `example.config.yml`.

On "default" parameter you must to write the value that you want the test return. For example with ping:

- Will return 256 if ping fail
- Will return 0 if ping success

If you want to handle ping error as good test result, put 256 on default value.

If a set of test has a header, define a table tape on the frontend. This mean that it has an specific number of columns. Check about the correct order for each component to fit the header.
The header must to be in the same order as the `default` and `params` options.

The `enabled` key just enable or disable the test.

### Dependencies to install

```bash
pip install pyyaml
pip install -U Flask
```


![Overview](img/simple.png)
