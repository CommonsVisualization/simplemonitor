'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('simpleMonitorApp')
  .controller('MainCtrl',  [ '$scope', 'reportService', '$location',
                function ($scope, reportService, $location) {

    reportService.get(function (r){
      parseReport(r)
    })

    $scope.tables = []

    $scope.location = $location.protocol() + '://' + $location.host() + ':' + $location.port() + '/report';

    var parseReport = function (r){
      console.log(r);
      // Set the title of the page
      $scope.title = "Report from " + new Date(r.endTime )
      $scope.hostname = r.hostname

      //For each key in report get who have array inside (so are test reports)
      for (var prop of Object.keys(r)){
        // console.log(r[prop] )
        // console.log("Is array" ,Array.isArray(r[prop]) )

        // If is an array create the table of the array
        if (Array.isArray(r[prop])) {
          // $scope.createTables(prop,r[prop])
          $scope.tables.push({ title : prop,
                                data : r[prop]})
          console.log("tables " , $scope.tables);
        }
      }

    }

    $scope.createTables = function (title,data){
      // $scope. console.log(data);

    }



    // this.awesomeThings = [
    //   'HTML5 Boilerplate',
    //   'AngularJS',
    //   'Karma'
    // ];
    //
    // $scope.matches = ""
    // $scope.selected = ""
    // inputsService.get(function (r) {
    //   $scope.inputRecords = r;
    // })
    //
    // $scope.selectInput = function (input){
    //   $scope.selected = input
    //   matchesservice.get(input.id, function (r) {
    //       $scope.matches = r;
    //   })
    // }
    //
    // $scope.selectMatch = function (match){
    //   if (confirm('Confirm that the selected song \n' +
    //               $scope.selected.title + $scope.selected.artist + $scope.selected.isrc + $scope.selected.duration +
    //               '\nmatch with:\n'+
    //               match.record.title + match.record.artist + match.record.isrc + match.record.duration )
    //     ){
    //
    //   }
    //   inputsService.select(match, $scope.selected, function (input){
    //     $scope.inputRecords = inputsService.delete(input)
    //     $scope.matches = "";
    //     console.log($scope.inputRecords);
    //   })
    // }

  }]);
