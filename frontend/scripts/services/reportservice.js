'use strict';

angular.module('simpleMonitorApp')
    .service('reportService', [ 'reportRemote',
      function (reportRemote) {
        this.get = function (cb){
          reportRemote.get().$promise
            .then (
              function (data){
                if (cb) cb(data)
              }
            )
        }
      }])
      .service('reportRemote', ['$resource',
      function($resource){
        var base = "report.json"
        return $resource(base, {}, {
          get: {
            method:'GET',
          }

      })
      }])
