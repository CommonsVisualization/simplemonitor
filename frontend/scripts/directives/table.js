'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:matchlist
 * @description
 * # matchlist
 */
angular.module('simpleMonitorApp')
  .directive('tabledirective', function () {
    return {
      templateUrl: 'scripts/directives/templates/tabletemplate.html',
      restrict: 'AEC',
      scope: {
        data : '=',
        cb : '=',
        hostname : '='
      },
      link: function (scope, element, attrs) {
        scope.oneRow = true
        scope.tableData = scope.data.data



        // For multiple rows of components
        // if (scope.data.title == "externals") {
        if (scope.data.data[0]['header'] ) {
          scope.oneRow = false
          var jsonHeader = scope.data.data[0]['header']

          // Todo: transform this to a model object

          // For the moment the headers accepted had categories:
          // header:
          //  subheader:
          //    - entries
          // Lets put all toguether to create a single array
          scope.header = []
          scope.lines = []
          // Get each table data. Start to 1 because 0 is the header
          for (var dataCount = 1; dataCount < scope.tableData.length; dataCount++) {
            var line = []
            // For each key of header
            for (var k in jsonHeader) {
              // we will use this loop to acces also to the data and put it on lines
              // Get each entry of the header to put it into the header array
              for (var h = 0; h < jsonHeader[k].length; h++) {
                if (dataCount == 1) {
                  scope.header.push(jsonHeader[k][h])
                }

                // Get each element to create the row of the table
                var name = scope.tableData[dataCount].testConfig.name;
                var result = scope.tableData[dataCount].result[k][h];
                var params = scope.tableData[dataCount].testConfig.params[k][h];
                var defa = scope.tableData[dataCount].testConfig.default[k][h];

                line.push({
                  "name" : name,
                  "result" : result,
                  "params" : params,
                  "defa" : defa
                })
              }
            }
            scope.lines.push(line)
          }
          console.log("final",scope.lines);

          // Add new header called component
          scope.header.splice(0, 0, "Component")

        }


      }
    };
  });
