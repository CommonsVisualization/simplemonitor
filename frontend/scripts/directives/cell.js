'use strict';

/**
 * @ngdoc directive
 * @name frontendApp.directive:matchlist
 * @description
 * # matchlist
 */
angular.module('simpleMonitorApp')
  .directive('cell', function () {
    return {
      templateUrl: 'scripts/directives/templates/celltemplate.html',
      restrict: 'AEC',
      // replace: true,
      scope: {
        data : '=',
        name : '=',
        desc : '=',
        default : '=',
        result : '=',
        disablesemaphore : '='
      },
      link: function (scope, element, attrs) {
        if (typeof scope.data !== 'undefined') {
          // console.log("NO undef!!!");
          scope.nam = scope.data.testConfig.name
          scope.des = scope.data.testConfig.desc
          scope.def = scope.data.testConfig.default
          scope.res = scope.data.result
        }
        else {
          // console.log("undef!!!");

          scope.nam = scope.name
          scope.des = scope.desc
          scope.def = scope.default
          scope.res = scope.result
        }
        // scope.sem = scope.enablesemaphore

        // console.log(
        //   scope.nam ,
        //   scope.des ,
        //   scope.def ,
        //   scope.res ,
        //   scope.disablesemaphore
        // );
      }
    };
  });
