from tests.testABC import TestABC
import os, json, copy


class Ping(TestABC):
    """Check if a host is open using ping utility
    :param  self.params['ipv4'] is the ip to check
    """

    def __init__(self, params):
        """In this case the params['ipv4'] contains"""
        self.params = params
        pass


    def run(self):
        """Implement run the test
        :return object with the results of the scannings
        """

        # stat = self.params.copy()
        # stat = dict(self.params)
        stat = copy.deepcopy(self.params)
        print(stat)
        if (self.params['ipv4']):
            for i  in range(len(self.params['ipv4'])):
                if (self.params['ipv4'][i]):
                    ip =  self.params['ipv4'][i]
                    temp = os.system("ping -c 2 " + ip)
                    stat['ipv4'][i] = str(temp)

        if (self.params['ipv6']):
            for i  in range(len(self.params['ipv6'])):
                if (self.params['ipv6'][i]):
                    ip =  self.params['ipv6'][i]
                    temp = os.system("ping6 -c 2 " + ip)
                    stat['ipv6'][i] = str(temp)
        return stat
