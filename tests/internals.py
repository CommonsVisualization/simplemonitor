from tests.testABC import TestABC
import os

class Systemctl (TestABC):
    """Test to check if a service is running using systemctl command
    :param self.params['service'] is the name of the service, for example nginx.service
    """

    def __init__(self, params):
        """In this case the params['service'] contains the name of the service"""
        self.params = params
        pass


    def run(self):
        """Implement run the test
        :return 0 if is runing or 768 if is not runing (check documentation for systemctl)
        """

        if (self.params['service']):
            c = 'systemctl status --no-pager ' + self.params['service']
            print("executing test", c)
            stat = os.system(c)
            return stat

        return -1

class Psef (TestABC):
    """Test if a program is runing using 'ps cax | grep' comand
    :param self.params['process'] is the name of the command to check. If
    """

    def __init__(self, params):
        """In this case the params['process'] contains the name of the process"""
        self.params = params
        pass

    def run(self):
        """Implement run the test
        :return 0 if is runing
        """

        if (self.params['process']):
            c = 'ps -ef | grep -w ' + self.params['process'] + ' | grep -v "grep" -q'
            print("executing test", c)
            stat = os.system(c)
            return stat

        return -1