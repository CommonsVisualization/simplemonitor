from enum import Enum


class TestsTypes(Enum):
    """Enum that define test types"""

    INTERNALS = "internals"
    EXTERNALS = "externals"