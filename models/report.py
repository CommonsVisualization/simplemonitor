import time
from tests.testTypes import TestsTypes
import json
import socket

class ReportEntry (object):
    def __init__(self, testConfig, result):
        self.testConfig = testConfig
        self.result = result

    def __repr__(self):
        return str(vars(self))

# Used to specify if a set of entries has common header
class ReportHeader (object):
    def __init__(self, header):
        self.header = header

    def __repr__(self):
        return str(vars(self))

class Report (object):

    def __init__(self):
        self.internals = [];
        self.externals = [];
        self.initTime = int(round(time.time() * 1000))
        self.hostname = socket.gethostname()
        self.endTime = self.initTime


    def addEntry (self, entry, type):
        if (type == TestsTypes.INTERNALS.name):
            self.internals.append(entry)
            # print (entry.__dict__)
            # print (json.dumps(entry.__dict__))

        elif (type == TestsTypes.EXTERNALS.name):
            self.externals.append(entry)

    def setEndTime (self, t = int(round(time.time() * 1000))):
        self.endTime = t

    def serialize (self):

        j = str(self.__dict__).replace("\'", "\"")
        # To solve JS convertion error
        j = j.replace("True", "\"true\"")
        j = j.replace("False", "\"false\"")
        j = j.replace("None", "\"None\"")

        dump = json.dumps(j)
        return json.loads(dump)

