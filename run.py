import yaml
import time
from tests import internals, externals
from tests.testTypes import TestsTypes
from models.report import Report, ReportEntry, ReportHeader

from flask import Flask
from flask import request
from flask import jsonify
from flask import send_from_directory



def getTest(testName, methodType):
    """Reflexive function that find for a function name if exist on the tests package
    :param testName: string with the test method name to find
    :param methodType: Enum of TestsTypes
    :return test object to isntantiate or None if not exists
        """
    testName = testName.capitalize()
    if (methodType == TestsTypes.INTERNALS.name):
        if (testName in dir(internals)):
            class_ = getattr(internals, testName)
            return class_

    elif (methodType == TestsTypes.EXTERNALS.name):
        if (testName in dir(externals)):
            class_ = getattr(externals, testName)
            return class_

    return None

def save(t, dest):
    print("Serializing to ", dest)
    f = open(dest, "w")
    f.write(t)


def createReport():
    # Run each test type tests (internal or external)
    report = Report()
    for testEntry in cfg:
        print cfg[testEntry]
        # Check if the yml entry is a test entry. If has the entry type it is
        if ('type' in cfg[testEntry]):
            # Get test type internal or external
            type = cfg[testEntry]["type"]
            # If the set of entries has header copy put it on the repor
            if ('header' in cfg[testEntry]):
                report.addEntry( ReportHeader(cfg[testEntry]["header"]) , type)
            # For each test...
            for test in cfg[testEntry]["tests"]:
                # Initialize stat test (the result of the test)
                stat = None
                # Check if the test is enabled
                if (test["enabled"] is True):
                    # Geat each test class to instantiate
                    tClass = getTest(test["type"], type)
                    print (type,": Looking for method", test["type"], "for", test["name"],":", tClass != None)
                    # If the program get a valid class to make the test
                    if (tClass != None):
                        # Instantiate the class with the params and run the test
                        stat = tClass(test["params"]).run()
                # Create a report entry and add it to the report
                report.addEntry( ReportEntry(test, stat) , type)
    # Set report end time
    report.setEndTime()
    # Save serialized data to a file
    serialized = report.serialize()
    save(serialized, cfg["jsonDest"])
    return serialized


app = Flask(__name__)
@app.route('/', defaults=dict(filename=None))
@app.route('/<path:filename>', methods=['GET', 'POST'])
def index(filename):
    filename = filename or 'index.html'
    if request.method == 'GET':
        return send_from_directory('frontend', filename)

    return jsonify(request.data)

@app.route('/report')
def callForReport():
    res = createReport()
    return res


def main():
    print ("Running tests... ")
    with open(".config.yml", 'r') as ymlfile:
        global cfg
        cfg = yaml.load(ymlfile)
    app.run(host='::', debug=1, port=cfg["port"])

if __name__ == '__main__':
    start_time = time.time()
    main( )
    print("--- %s seconds ---" % (time.time() - start_time))